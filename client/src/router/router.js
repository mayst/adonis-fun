import Vue from 'vue'
import Router from 'vue-router'
// import Main from './views/Main.vue';
// import Profile from './views/Profile.vue';
// import Chat from './views/Chat.vue';
// import Error from '@/views/Error.vue'

Vue.use(Router)

const lazyLoad = path => () => import(`@/views/${path}.vue`)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'main',
      // meta: { requiresAuth: true },
      component: lazyLoad('Main')
    },
    {
      path: '/login',
      name: 'login',
      component: lazyLoad('Login')
    },
    {
      path: '/profile',
      name: 'profile',
      meta: { requiresAuth: true }
      // component: lazyLoad('Profile'),
    },
    {
      path: '/chat',
      name: 'chat',
      meta: { requiresAuth: true }
      // component: lazyLoad('Chat'),
    },
    {
      path: '/about',
      name: 'about'
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      /* webpackChunkName: "about" */
      // component: lazyLoad('About')
    }
    // {
    //   path: '*',
    // component: lazyLoad('Error')
    // },
  ]
})
