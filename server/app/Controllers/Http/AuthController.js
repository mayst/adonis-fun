'use strict'

class AuthController {
  async login ({ auth, request }) {
    const { email, password } = request.all()
    await auth.attempt(email, password)

    response.send('User successfully logged in')
  }
}

module.exports = AuthController
